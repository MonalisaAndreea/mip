package util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

/**
 * 
 * @In DatabaseUtil: connection and transaction CRUD for Animal, PersonalMedical
 *     and Programare
 */

public class DatabaseUtil {

	// SINGLETON
	public static DatabaseUtil instance = new DatabaseUtil();

	public static DatabaseUtil getInstance() {
		return instance;
	}

	public static void setInstance(DatabaseUtil instance) {
		DatabaseUtil.instance = instance;
	}

	// connection
	public static EntityManagerFactory entityManagerFactory;

	// transaction
	public static EntityManager entityManager;

	// Variables Initialization
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop-JPA");
		entityManager = entityManagerFactory.createEntityManager();
	}

	// The save functions from Animal, PersonalMedical and Programare
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	public void savePersonalMedical(Personalmedical personal) {
		entityManager.persist(personal);
	}

	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	// function that stops the entity, the connection
	public void closeEntityManager() {
		entityManager.close();
	}

	// function that prints the results from the database through a list
	public void printAllAnimals() {
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal: " + animal.getName() + " - " + animal.getAge() + " years old "
					+ animal.getType() + "  " + animal.getCategory() + " with " + animal.getNumberOfAppointments()
					+ " appointments " + "has ID:" + animal.getIdAnimal());
		}
	}

	public void printAllPersonals() {
		List<Personalmedical> results = entityManager
				.createNativeQuery("Select * from PetShop.Personalmedical", Personalmedical.class).getResultList();
		for (Personalmedical personal : results) {
			System.out.println("Personal " + personal.getName() + " : " + personal.getSpecialization() + ", "
					+ personal.getNumberOfAppointments() + " appointments " + " has ID:"
					+ personal.getIdPersonalMedical());
		}
	}

	public void printAllAppointments() {
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		for (Programare programare : results) {
			System.out.println(
					"The appointment between: " + programare.getNameAnimal() + " and " + programare.getNameDoctor()
							+ " - " + programare.getTimeAndDate() + " has ID: " + programare.getIdProgramare());
		}
	}

	// Functions for Animal, PersonalMedical and Programare that return objects of
	// their type through id
	public Animal findAnimal(int id) {

		Animal animal = entityManager.find(Animal.class, id);
		return animal;
	}

	public Personalmedical findPersonalMedical(int id) {

		Personalmedical personalMedical = entityManager.find(Personalmedical.class, id);
		return personalMedical;
	}

	public void printProgramare(int id) {

		Programare programare = entityManager.find(Programare.class, id);
		System.out.println("Date:" + programare.getTimeAndDate() + ", animal:" + programare.getAnimal().getName()
				+ " and personal medical:" + programare.getPersonalmedical().getName() + "with ID's appointment: "
				+ programare.getIdProgramare());
	}

	// HASHSET
	public HashSet<Animal> animalHashSet() {
		HashSet<Animal> animalList = (HashSet<Animal>) entityManager.createQuery("SELECT a FROM Animal a", Animal.class)
				.getResultList();
		return animalList;
	}

	public HashSet<Programare> appointmentHashSet() {
		HashSet<Programare> appointmentList = (HashSet<Programare>) entityManager
				.createQuery("SELECT p FROM Programare p", Programare.class).getResultList();
		return appointmentList;
	}
	
	//HASHMAP
	/*public HashMap<Integer,Programare> printAllAppointmentsForAnAnimal(Animal a, int id){
		HashMap<Integer, Programare> animalAppointments = (HashMap<id, Programare>) entityManager.
				createQuery("SELECT appointments FROM Animal a", Programare.class).getResultList();
		return animalAppointments;
	}*/
	 

	// returns a list of objects(Animal or Programare)
	public List<Animal> animalList() {
		List<Animal> animalList = (List<Animal>) entityManager.createQuery("SELECT a FROM Animal a", Animal.class)
				.getResultList();
		return animalList;
	}

	public List<Programare> appointmentList() {
		List<Programare> appointmentList = (List<Programare>) entityManager
				.createQuery("SELECT p FROM Programare p", Programare.class).getResultList();
		return appointmentList;
	}

	// Delete functions for Animal, PersonalMedical and Programare through id
	public void deleteAnimalId(int id) {
		Animal animal = entityManager.find(Animal.class, id);
		entityManager.remove(animal);
	}

	public void deletePersonalMedicalId(int id) {
		Personalmedical personal = entityManager.find(Personalmedical.class, id);
		entityManager.remove(personal);
	}

	public void deleteProgramareId(int id) {
		Programare programare = entityManager.find(Programare.class, id);
		entityManager.remove(programare);
	}

	// Delete functions
	public void deleteAnimal(Animal animal) {
		entityManager.remove(animal);
	}

	public void deletePersonalMedical(Personalmedical personal) {
		entityManager.remove(personal);
	}

	public void deleteProgramare(Programare progrmare) {
		entityManager.remove(progrmare);
	}

	/**
	 * 
	 * @param idAnimal
	 * @param name
	 * @param category
	 * @param type
	 * @param age
	 * @param numberOfAppointments
	 * 
	 * @return Update functions through id
	 */
	public Animal updateAnimal(int idAnimal, String name, String category, String type, int age,
			int numberOfAppointments) {
		Animal animal = entityManager.find(Animal.class, idAnimal);
		animal.setName(name);
		animal.setCategory(category);
		animal.setType(type);
		animal.setAge(age);
		animal.setNumberOfAppointments(numberOfAppointments);
		return animal;
	}

	public Personalmedical updatePersonalMedical(int idPersonalMedical, String name, String specialization,
			int numberOfAppointments) {
		Personalmedical personal = entityManager.find(Personalmedical.class, idPersonalMedical);
		personal.setIdPersonalMedical(idPersonalMedical);
		personal.setName(name);
		personal.setSpecialization(specialization);
		personal.setNumberOfAppointments(numberOfAppointments);
		return personal;
	}

	public Programare updateProgramare(int idProgramare, String nameAnimal, String nameDoctor, String timeAndDate) {
		Programare programare = entityManager.find(Programare.class, idProgramare);
		programare.setIdProgramare(idProgramare);
		programare.setNameAnimal(nameAnimal);
		programare.setNameDoctor(nameDoctor);
		programare.setTimeAndDate(timeAndDate);
		return programare;
	}

	// Sorting function
	public void sortAppointments() {
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		results.sort(Comparator.comparing(Programare::getTimeAndDate));
		for (Programare programare : results) {
			System.out.println("Appointments Id : " + programare.getIdProgramare() + " , name of the animal: "
					+ entityManager.find(Animal.class, programare.getIdProgramare()).getName()
					+ " and the name of doctor "
					+ entityManager.find(Personalmedical.class, programare.getIdProgramare()).getName()
					+ " , with data:" + programare.getTimeAndDate());

		}
	}

	// LAMBDA
	public void sortAppointmentsWithLambda() {
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		Collections.sort(results, (r1, r2) -> r1.getTimeAndDate().compareTo(r2.getTimeAndDate()));
		for (Programare programare : results) {
			System.out.println("Appointments Id : " + programare.getIdProgramare() + " , name of the animal: "
					+ entityManager.find(Animal.class, programare.getIdProgramare()).getName()
					+ " and the name of doctor "
					+ entityManager.find(Personalmedical.class, programare.getIdProgramare()).getName()
					+ " , with data:" + programare.getTimeAndDate());

		}
	}

	public void sortAnimalsWithLambda() {
		List<Animal> results = entityManager.createNativeQuery("Select * from Animal", Animal.class).getResultList();
		Collections.sort(results, (r1, r2) -> r1.getName().compareTo(r2.getName()));
		for (Animal a : results) {
			System.out.println(a.getName());
		}
	}

	public void sortPersonalsWithLambda() {
		List<Personalmedical> results = entityManager
				.createNativeQuery("Select * from PersonalMedical", Personalmedical.class).getResultList();
		Collections.sort(results, (r1, r2) -> r1.getName().compareTo(r2.getName()));
		for (Personalmedical p : results) {
			System.out.println(p.getName());
		}
	}

	// GENERIC
	/**
	 * 
	 * @param object this generic function delete the object(Animal, PersonalMedical
	 *               or Appointments)
	 */
	public <T> void delete(T object) {
		entityManager.remove(object);
	}
}
