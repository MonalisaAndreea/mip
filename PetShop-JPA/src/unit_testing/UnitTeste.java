package unit_testing;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.List;

import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

public class UnitTeste {

	// it's tested whether two variables belong to the same object
	@Test
	public void testSameObject() {

		Animal a = new Animal();
		a.setAge(4);
		a.setCategory("Cat");
		assertSame("The variables are belong to the same object", a.getAge(), a.getCategory());
	}

	// testing the update functions
	@Test
	public void testUpdadeAnimal() {

		DatabaseUtil db = new DatabaseUtil();
		Animal a = new Animal();
		a.setAge(2);
		int age = a.getAge();
		a.setName("Ara");
		a.setIdAnimal(13);
		a.setCategory("Guineea Pig");
		a.setType("Peruvian");
		a.setNumberOfAppointments(0);
		a = db.updateAnimal(13, "Ara", "Guineea Pig", "Peruvian", 3, 7);
		assertEquals("It's correct!", age, a.getAge());

	}

	@Test
	public void testUpdadePersonalMedical() {

		DatabaseUtil db = new DatabaseUtil();
		Personalmedical p = new Personalmedical();
		p.setIdPersonalMedical(23);
		p.setName("Popovi");
		p.setNumberOfAppointments(198);
		p.setSpecialization("General");
		int id = p.getIdPersonalMedical();
		p = db.updatePersonalMedical(24, "Popovi", "General", 198);
		assertNotEquals(id, p.getIdPersonalMedical());

	}

	@Test
	public void testAvailableAnimal() {

		Animal a = new Animal();
		a.setName("Roko");
		a.setAge(2);
		a.setIdAnimal(56);
		a.setNumberOfAppointments(12);
		a.setCategory("dog");
		a.setType("Ciobanesc German");

		assertNotNull(a);
	}

	@Test
	public void testAvailablePersonalMedical() {

		Personalmedical p = new Personalmedical();
		p.setIdPersonalMedical(2);
		assertNotNull(p.getIdPersonalMedical());
	}

	@Test
	public void testAvailableAppointments() {

		Programare p = new Programare();
		assertNull(p);

	}

	@Test
	public void testUpdadePersonal() {

		DatabaseUtil db = new DatabaseUtil();
		Personalmedical p = new Personalmedical();
		p.setIdPersonalMedical(29);
		p.setName("Popovici");
		p.setNumberOfAppointments(18);
		p.setSpecialization("General");
		int id = p.getIdPersonalMedical();
		p = db.updatePersonalMedical(29, "Popovici", "General", 198);
		assertTrue("It's correct", id == p.getIdPersonalMedical());
		// assertNotEquals(id,p.getIdPersonalMedical());

	}

	@Test
	public void testSortAnimalsWithLambda() {
		DatabaseUtil db = new DatabaseUtil();
		List<Animal> results = db.entityManager.createNativeQuery("Select * from Animal", Animal.class).getResultList();
		Collections.sort(results, (r1, r2) -> r1.getName().compareTo(r2.getName()));
		for (Animal a : results) {
			System.out.println(a.getName());
		}

		fail("It's wrong");
	}

	@Test
	public void testFindPersonalMedical(int id) {

		DatabaseUtil db = new DatabaseUtil();
		Personalmedical personalMedical = db.entityManager.find(Personalmedical.class, id);
		Personalmedical m = personalMedical;
		fail();
	}

	@Test
	public void testStartTransaction() {
		DatabaseUtil db = new DatabaseUtil();
		db.entityManager.getTransaction().begin();
		fail();
	}

	@Test
	public void testCloseEntityManager() {
		DatabaseUtil db = new DatabaseUtil();
		db.entityManager.close();
		fail();
	}

}
