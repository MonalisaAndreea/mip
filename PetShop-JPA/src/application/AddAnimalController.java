package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

/**
 * 
 * Controller for adding appointments
 *
 */
public class AddAnimalController {

	@FXML
	private TextField idField;
	@FXML
	private TextField nameField;
	@FXML
	private TextField typeField;
	@FXML
	private TextField ageField;
	@FXML
	private TextField categoryField;

	@FXML
	private Button OK;
	@FXML
	private Button cancel;

	private Stage dialogStage;

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	@FXML
	void OKButtonClicked(ActionEvent e) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();

		Animal animal = new Animal();
		animal.setIdAnimal(Integer.parseInt(idField.getText()));
		animal.setName(nameField.getText());
		animal.setType(typeField.getText());
		animal.setAge(Integer.parseInt(ageField.getText()));
		animal.setCategory(categoryField.getText());
		db.startTransaction();
		db.saveAnimal(animal);
		db.commitTransaction();
		db.closeEntityManager();

		dialogStage.close();
	}

	@FXML
	void cancelButtonClicked(ActionEvent e) {
		dialogStage.close();
	}
}
