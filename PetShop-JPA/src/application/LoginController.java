package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController{

	//private String user = "Monalisa";
	//private String password = "0000";

	@FXML
	private Button loginButton;
		
	@FXML
	private Label statusLabel;
		
	@FXML
	private TextField usernameField;
		
	@FXML
	private PasswordField passwordField;

	/*@FXML 
	public void loginButton() throws Exception{
		if(userField.getText().equals("Monalisa") && passwordField.getText().equals("0000"))
		{
			statusLabel.setText("Login succeded!");
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/application/Main.fxml"));
			Scene scene = new Scene(root,500,500);
			scene.getStylesheets().add(getClass().getResource("/application/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			} else {
				statusLabel.setText("Invalid login! Try again!");
				}
		}*/
	
	public void LoginButton() throws Exception {
		if(usernameField.getText().equals("Monalisa") && passwordField.getText().equals("0000")) {
			statusLabel.setText("Login succeded!");
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/application/Main.fxml"));
			Scene scene = new Scene(root, 800, 800);
			scene.getStylesheets().add(getClass().getResource("/application/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} else {
			statusLabel.setText("Login failed! Try again!");
		}
	}
}
