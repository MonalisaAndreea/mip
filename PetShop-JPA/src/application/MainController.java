package application;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.Main;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

/**
 * 
 * @author user
 *
 */

public class MainController implements Initializable {

	private Main main;

	@FXML
	private ListView<String> listView;
	@FXML
	private ListView<String> animalDetailsListView;

	@FXML
	private Button addAppointment;
	@FXML
	private Button deleteAppointment;
	@FXML
	private Button updateAppointment;
	@FXML
	private Button insertAnimal;

	List<Animal> animals;

	@FXML
	void insertAnimalButtonClicked() throws Exception {
		main.showAddStageTwo();
		populateListView();
	}

	@FXML
	void addAppoinmentClicked() throws Exception {
		main.showAddStage();
		populateListView();
	}

	@FXML
	void deleteAppoinmentClicked(ActionEvent e) {
		System.out.println("test delete");
	}

	@FXML
	void updateAppoinmentClicked(ActionEvent e) {
		System.out.println("test update");
	}

	public void populateListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();

		List<Animal> animalDBList = (List<Animal>) db.animalList();
		this.animals = animalDBList;

		List<Programare> appointmentDBList = (List<Programare>) db.appointmentList();
		ObservableList<Programare> appointmentList = FXCollections.observableArrayList(appointmentDBList);
		listView.setItems(getAppointmentDateObservableList(appointmentList));
		listView.refresh();

		db.closeEntityManager();
	}

	public ObservableList<String> getAppointmentDateObservableList(List<Programare> appointments) {

		appointments.sort(Comparator.comparing(Programare::getTimeAndDate));
		ObservableList<String> date = FXCollections.observableArrayList();
		for (Programare p : appointments) {
			date.add("Date: " + p.getTimeAndDate());
		}
		return date;
	}

	@FXML
	void clickedListView() {

		int index = listView.getSelectionModel().getSelectedIndex();
		List<String> animalsDetails = getAnimalObservableList(this.animals);

		ObservableList<String> animalString = FXCollections.observableArrayList();
		animalString.add(animalsDetails.get(index));
		animalDetailsListView.setItems(animalString);
		animalDetailsListView.refresh();
	}

	public ObservableList<String> getAnimalObservableList(List<Animal> animals) {
		ObservableList<String> animalsList = FXCollections.observableArrayList();
		for (Animal a : animals) {
			animalsList.add("Name:     " + a.getName() + "\nAge:        " + a.getAge() + "\n Type:    " + a.getType()
					+ "\n Category:       " + a.getCategory());
		}
		return animalsList;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateListView();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
