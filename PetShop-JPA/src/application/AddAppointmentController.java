package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

/**
 * 
 * Controller for adding appointments
 *
 */

public class AddAppointmentController {

	@FXML
	private TextField animalIdField;
	@FXML
	private TextField medicalStaffIdField;
	@FXML
	private TextField idField;
	@FXML
	private TextField dateField;

	@FXML
	private Button OK;
	@FXML
	private Button cancel;

	private Stage dialogStage;

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	@FXML
	void OKButtonClicked(ActionEvent e) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();

		Programare programare = new Programare();
		Animal animal = db.findAnimal(Integer.parseInt(animalIdField.getText()));
		programare.setAnimal(animal);
		Personalmedical personalMedical = db.findPersonalMedical(Integer.parseInt(medicalStaffIdField.getText()));
		programare.setPersonalmedical(personalMedical);
		programare.setIdProgramare(Integer.parseInt(idField.getText()));
		programare.setTimeAndDate(dateField.getText());
		db.startTransaction();
		db.saveProgramare(programare);
		db.commitTransaction();
		db.closeEntityManager();

		dialogStage.close();
	}

	@FXML
	void cancelButtonClicked(ActionEvent e) {
		dialogStage.close();
	}
}
