package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the personalMedical database table.
 * 
 */

@Entity
@NamedQuery(name = "Personalmedical.findAll", query = "SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPersonalMedical;

	private String name;

	@Column(name = "number_of_appointments")
	private int numberOfAppointments;

	private String specialization;

	// bi-directional many-to-one association to Programare
	@OneToMany(mappedBy = "personalmedical")
	private List<Programare> programares;

	// Constructor
	public Personalmedical() {
	}

	// Getters and setters for PersonalMedic class
	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfAppointments() {
		return this.numberOfAppointments;
	}

	public void setNumberOfAppointments(int numberOfAppointments2) {
		this.numberOfAppointments = numberOfAppointments2;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}