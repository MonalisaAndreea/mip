package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the animal database table.
 * 
 */

@Entity
@NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAnimal;

	private int age;

	private String category;

	private String name;

	@Column(name = "number_of_appointments")
	private int numberOfAppointments;

	private String type;

	// bi-directional many-to-one association to Programare
	@OneToMany(mappedBy = "animal")
	private List<Programare> programares;

	// Constructor
	public Animal() {
	}

	// Getters and setters for Animal class
	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age2) {
		this.age = age2;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfAppointments() {
		return this.numberOfAppointments;
	}

	public void setNumberOfAppointments(int numberOfAppointments2) {
		this.numberOfAppointments = numberOfAppointments2;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

}