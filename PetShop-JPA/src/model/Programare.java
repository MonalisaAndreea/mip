package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the programare database table.
 * 
 */

@Entity
@NamedQuery(name = "Programare.findAll", query = "SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idProgramare;

	@Column(name = "name_animal")
	private String nameAnimal;

	@Column(name = "name_doctor")
	private String nameDoctor;

	@Column(name = "time_and_date")
	private String timeAndDate;

	// bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name = "idAnimal")
	private Animal animal;

	// bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name = "idPersonalMedical")
	private Personalmedical personalmedical;

	// Constructor
	public Programare() {
	}

	// Getters and setters for Programare class
	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public String getNameAnimal() {
		return this.nameAnimal;
	}

	public void setNameAnimal(String nameAnimal) {
		this.nameAnimal = nameAnimal;
	}

	public String getNameDoctor() {
		return this.nameDoctor;
	}

	public void setNameDoctor(String nameDoctor) {
		this.nameDoctor = nameDoctor;
	}

	public String getTimeAndDate() {
		return this.timeAndDate;
	}

	public void setTimeAndDate(String i) {
		this.timeAndDate = i;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}