package main;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.application.Application;

import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import application.AddAppointmentController;
import application.AddAnimalController;
import javafx.stage.Modality;
import javafx.stage.Window;

/**
 * Workbench: Animal, PersonalMedical, Programari
 * 
 */

public class Main extends Application {
	private static Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/Login.fxml"));
			//BorderPane root = FXMLLoader.load(getClass().getResource("/application/Main.fxml"));
			Scene scene = new Scene(root, 450, 450);
			scene.getStylesheets().add(getClass().getResource("/application/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showAddStage() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/application/AddAppointment.fxml"));
		BorderPane addNewAppointment = loader.load();
		
		Stage addDialogStage = new Stage();
		addDialogStage.setTitle("Add New Appointment");
		addDialogStage.initModality(Modality.WINDOW_MODAL);
		addDialogStage.initOwner(primaryStage);
		Scene scene = new Scene(addNewAppointment);
		addDialogStage.setScene(scene);
		
		AddAppointmentController controller = loader.getController();
        controller.setDialogStage(addDialogStage);
        
		addDialogStage.showAndWait();
	}
	
	public static void showAddStageTwo() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/application/AddAnimal.fxml"));
		BorderPane insertAnimal = loader.load();
		
		Stage addDialogStage = new Stage();
		addDialogStage.setTitle("Insert Animal");
		addDialogStage.initModality(Modality.WINDOW_MODAL);
		addDialogStage.initOwner(primaryStage);
		Scene scene = new Scene(insertAnimal);
		addDialogStage.setScene(scene);
		
		AddAnimalController controller = loader.getController();
        controller.setDialogStage(addDialogStage);
        
		addDialogStage.showAndWait();
	}
	
	public static void main(String[] args) {

		launch(args);
		/*
		 * DatabaseUtil dbUtil = new DatabaseUtil(); dbUtil.setUp();
		 * dbUtil.startTransaction();
		 * 
		 * //Create Animal /* Animal animal=new Animal(); animal.setIdAnimal(5);
		 * animal.setName("Sara"); animal.setAge(2); animal.setCategory("dog");
		 * animal.setType("Chow Chow"); animal.setNumberOfAppointments(1);
		 * dbUtil.saveAnimal(animal);
		 */
		// Update
		/*
		 * Animal ani = dbUtil.updateAnimal(5, "Bursuc", "dog", "mioritic shepherd", 9,
		 * 13); dbUtil.saveAnimal(ani);
		 */
		// dbUtil.commitTransaction();

		// Afisare
		// dbUtil.printAllAnimals();

		// Create Personal medical
		/*
		 * DatabaseUtil dbUtil = new DatabaseUtil(); dbUtil.setUp();
		 * dbUtil.startTransaction(); Personalmedical personal=new Personalmedical();
		 * personal.setIdPersonalMedical(5); personal.setName("Maria");
		 * personal.setSpecialization("general"); personal.setNumberOfAppointments(157);
		 * dbUtil.savePersonalMedical(personal); /* delete
		 * dbUtil.deletePersonalMedicalId(6);
		 */
		// dbUtil.commitTransaction();
		// dbUtil.printAllPersonals();

		//DatabaseUtil dbUtil = new DatabaseUtil();
		//dbUtil.setUp();
		//dbUtil.startTransaction();
		/*
		 * Programare programare=new Programare(); programare.setIdProgramare(4);
		 * programare.setNameAnimal("Kiti"); programare.setNameDoctor("Bob");
		 * programare.setTimeAndDate("2018-01-10"); dbUtil.saveProgramare(programare);
		 * dbUtil.commitTransaction(); dbUtil.printAllAppointments();
		 * dbUtil.closeEntityManager();
		 */

		//dbUtil.sortAppointments();
		
		
	}

	
	
}

	
	