package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class Echoer extends Thread {

	private Socket socket;
		
	public Echoer(Socket socket) {
		this.socket = socket;
	}
		
	@Override
	public void run() {
		
		String username="Monalisa";
		String password="0000";
		try {
			BufferedReader input = new BufferedReader(
			new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
				
			while(true) {
				
				String echoString = input.readLine();
				System.out.println("Input's client: " + echoString);
				if(echoString.equals("exit")) {
					break;
				   }
				else {
					if(echoString.equals(username)) output.println("The server received username");
					else { 
						if(echoString.equals(password)) output.println("The server received password");
						else output.println("The server received the data: " + echoString);
					}
				}
				
				//output.println(echoString + " From Server");
				//output.println("The server received the data: " + echoString);
				}
			}catch (IOException e) {
				System.out.println("Error " + e.getMessage());
				} finally {
					try {
						socket.close();
						} catch (IOException e) {
							e.printStackTrace();
							}
					}
		}
}
